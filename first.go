package main

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
)

func first_test () {
    data, err := ioutil.ReadFile("hard.json")
    if err != nil {
        fmt.Println(err)
        return
    }

    var arr [][]int

    err = json.Unmarshal(data, &arr)
    if err != nil {
        fmt.Println(err)
        return
    }

    max, result, i, j := 0, 0, 0, 0

    save_j := 0

    for i = 0; i < len(arr); i++ {
        max = 0
        if i == 0 {
            max = arr[i][j]
        } else {
            for j = save_j; j <= save_j + 1; j++ {
                if arr[i][j] > max && (save_j == j || save_j + 1 == j){
                    max = arr[i][j]
                    save_j = j
                }
            }
        }
        result += max
    }

    fmt.Println(result)
}

func main() {
    first_test()
}
