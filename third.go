package main

import (
    "strings"
    "net/http"
    // "io"
    "github.com/gin-gonic/gin"
	"io/ioutil"
)

func Split(r rune) bool {
    return r == ',' || r == '.' || r == ' '
}

func dup_count(array []string) map[string]int {
    count := make(map[string]int)
    for _, item := range array {
        _, exist := count[item]

        if exist {
            count[item] += 1
        } else {
            count[item] = 1
        }
    }
    return count
}

type Beef struct {
    Beef     map[string]int  `json:"beef"`
}

func getBeefSummary(c *gin.Context) {
    // https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text
    url := "https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text"
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

    bodyString := strings.ReplaceAll(string(body), "\n", "")

    splited_body := strings.FieldsFunc(string(bodyString), Split)
    dup_body := dup_count(splited_body)

    c.IndentedJSON(http.StatusOK, &Beef{dup_body})
}

func main() {
    router := gin.Default()

    router.GET("/beef/summary", getBeefSummary)

    router.Run("localhost:8080")
}
