package main

import (
    "fmt"
    "strconv"
)

func sec_test () {
    var encoded string

    fmt.Println("Enter encoded (L R =)")
    fmt.Scanf("%s", &encoded)

    result := ""

    for i := 0; i < len(encoded); i++ {

        if encoded[i] == 'L' {
            count := 1
            for j := 1; true; j++ {
                if encoded[i+j] != 'L' {
                    break;
                } else {
                    count++
                }
            }

            i += count - 1
            for k := count; k >= 0; k-- {
                result+=string(strconv.Itoa(k))
            }
        } else if (encoded[i] == 'R') {
            count := 1
            for j := 1; true; j++ {
                if encoded[i+j] != 'R' {
                    break;
                } else {
                    count++
                }
            }
            i += count - 1
            for k := 1; k <= count; k++ {
                result+=string(strconv.Itoa(k))
            }
        } else if (encoded[i] == '=') {
            result+=string(result[len(result) - 1])
        }
	}
    fmt.Println(result)
}

func main() {
    sec_test()
}
